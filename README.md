
URL https://dev.bukkit.org/projects/autocrafter
source code https://github.com/andrepl/Autocrafter

Autocrafter

Autocrafter lets you turn a regular minecraft dropper into an automatic redstone-powered crafting table.
By attaching an Item Frame to a Dropper and placing an item in the frame, it will attempt to craft and dispense
a copy of that item every time it recieves a redstone signal.
Features

    Multi-world support
    per-recipe creation permissions.

Commands

    None

Permissions

    'autocrafter.create.*' - players with this permission can place any item in the frame attached to a dropper.

update to mine 1.13.2